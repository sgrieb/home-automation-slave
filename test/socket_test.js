if(process.env.NODE_ENV){
    config = require('../config.'+ process.env.NODE_ENV);
}
else{
    config = require('../config.global');
}

var assert = require('chai').assert;
var WebSocket = require('ws');

describe('socket', function() {
    var ws,
        socket;

    before(function() {
        // runs before all tests in this block
        socket = require('../server/socket');

        //start the socket service
        socket.init(config);

    });

    beforeEach(function() {
        // runs before each test in this block
        ws = new WebSocket('ws://localhost:' + config.socketPort);
    });

    describe('common', function() {
        it('should connect', function (done) {
            ws.on('open', function open() {
                //ws.send('1st');
                done();
            });
        });

        it('should respond to a bad type', function (done) {
            ws.on('open', function open() {
                ws.on('message', function incoming(message) {
                    console.log('test received: %s', message);

                    done();
                });


                ws.send(JSON.stringify({type:'Confused'}));

            });
        });

        it('should respond to a good type', function (done) {
            ws.on('open', function open() {
                ws.on('message', function incoming(message) {
                    console.log('test received: %s', message);

                    done();
                });

                ws.send(JSON.stringify({type:'login'}));
            });
        });
    });

    describe('login', function() {

        it('should respond to a good login type', function (done) {
            ws.on('open', function open() {
                ws.on('message', function incoming(message) {
                    console.log('test received: %s', message);

                    done();
                });


                ws.send(JSON.stringify({type:'login', email:'poop@crap.com', password:'ilikenakedpeople'}));

            });
        });

        it('should respond to a valid login', function (done) {
            ws.on('open', function open() {
                ws.on('message', function incoming(message) {
                    try{
                        message = JSON.parse(message);
                        assert(message.token, 'No Token received');
                        console.log('Got token:' + message.token);
                        done();
                    }
                    catch(e){
                        //fail if we get here
                        assert(true, e.message);
                        done();
                    }
                });


                ws.send(JSON.stringify({type:'login', email:'poop@poop.poop', password:'ilikenakedpeople'}));

            });
        });
    });

});